<?php get_header(); ?>

<main class="container white-bg">
    <div class="row">
        <div class="col-12 text-center">
            <h2>
                <?php wp_title( $sep = '' ) ?>
            </h2>
        </div>

        <div class="post-card-wrapper col-12">
            <div class="row">
                <?php if ( have_posts() ): ?>
                    <?php while ( have_posts() ): ?>
                        <?php the_post(); ?>

                        <div class="col-lg-6 p-0">
                            <div class="post-card">
                                <div class="post-card-description">
                                    <a href="<?php echo get_permalink() ?>">
                                        <h4 class="post-card-heading"><?php the_title(); ?></h4>
                                    </a>

                                    <div class="post-card-details">
                                        <span><img class="post-card__date" src="<?php echo get_template_directory_uri(); ?>/assets/icons/clock.svg" alt=""><?php the_date(); ?></span>

                                        <span><img class="post-card__category" src="<?php echo get_template_directory_uri(); ?>/assets/icons/tags.svg" alt=""><?php the_category( ',&nbsp;' ); ?></span>
                                    </div>
                                </div>
                                
                                <a href="<?php the_permalink() ?>">
                                    <div class="post-card-img">
                                        <picture>
                                            <?php if( get_the_post_thumbnail() ): ?>
                                                <img src="<?php the_post_thumbnail_url( $size = 'thumbnail' ) ?>" alt="">
                                                <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-placeholder.svg' ?>" alt="">
                                            <?php endif; ?>
                                        </picture>
                                    </div>
                                </a>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="post-pagination">

        <?php
            $pagination = paginate_links( array(
                'mid_size' => 1,
                'prev_text' => '<img class="post-pagination__left" src="'. get_template_directory_uri() . '/assets/icons/arrow-left.svg" alt="">',
                'next_text' => '<img class="post-pagination__right" src="'. get_template_directory_uri() . '/assets/icons/arrow-right.svg" alt="">',
            ) );

            echo $pagination;
        ?>

        </div>
    </div>
</main>

<?php get_footer(); ?>