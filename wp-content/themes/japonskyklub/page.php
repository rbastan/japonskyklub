<?php
    get_header();

    if (have_rows('content')):
        while(have_rows('content')): the_row();
            if(get_row_layout() == 'hero-image'):
                include 'modules/hero.php';
            endif;
        endwhile;
    endif;

    include 'content.php';

    if (have_rows('content')):
        while(have_rows('content')): the_row();
            if(get_row_layout() == 'map'): 
                include 'modules/map.php';
            endif;
        endwhile;
    endif;

    get_footer();
?>