<?php

/* ------------------------------------
    Opravdový title stránky z backendu - používá se pro headingy
------------------------------------ */
    function real_title() {
        if (is_front_page()) {
            $real_title = "";
        } else if (is_home()) {
            $real_title = get_the_title(get_option('page_for_posts', true));
        } else if (is_category()) {
            $real_title = single_cat_title();
        } else {
            $real_title = get_the_title();
        }

        echo $real_title;
    }


/* ------------------------------------
    Title stránky - používá se pro <title> a OG tagy
------------------------------------ */
    function heading_title() {
        if (is_front_page()) {
            bloginfo('name');
        } else if (is_home()) {
            echo get_the_title(get_option('page_for_posts', true)) . " · ";
            bloginfo('name');
        } else if (is_category()) {
            echo single_cat_title() . " · ";
            bloginfo('name');
        } else {
            echo get_the_title() . " · ";
            bloginfo('name');
        }
    }


/* ------------------------------------
    SEO & Meta tagy
------------------------------------ */

    function meta($property) {
        $meta = get_field('meta');
        $meta_name = $meta[$property];

        if (strlen($meta_name) !== 0) {
            echo $meta_name;
        } else {
            switch ($property) {
                case ($property === "meta_title"):
                case ($property === "og_title"):
                    heading_title();
                break;
                case ($property === "meta_description"):
                case ($property === "og_description"):
                    bloginfo('description');
                break;
            }
        }
    }

    function og_image() {
        $og_image = get_field('meta')['og_image']['url'];
        
        if (strlen($og_image) !== 0) {
            echo $og_image;
        }
    }
?>