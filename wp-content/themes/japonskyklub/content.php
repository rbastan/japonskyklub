<main class="container white-bg">
    <?php
        if (have_rows('content')):
            while(have_rows('content')): the_row();
                if(get_row_layout() == 'image-right'):
                    include 'modules/image-right.php';
                endif;
                if(get_row_layout() == 'image-left'):
                    include 'modules/image-left.php';
                endif;
                if(get_row_layout() == 'no-image'):
                    include 'modules/no-image.php';
                endif;
                if(get_row_layout() == 'accordion'):
                    include 'modules/accordion.php';
                endif;
                if(get_row_layout() == 'events'):
                    include 'modules/events.php';
                endif;
            endwhile;
        endif;
    ?>
</main>