<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-12 hero-logo-wrapper">
            <div class="hero-logo">
                <a href="<?php echo get_bloginfo('url')?>">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-bile.svg' ?>">
                </a>
            </div>
        </div>
    </div>
</div>

<?php if ( have_posts() ): ?>
    <?php while ( have_posts() ): ?>
        <?php the_post(); ?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12 hero post">
                    <picture>
                        <source media="(min-width: 1920px)" srcset="<?php the_post_thumbnail_url('desktop-img') ?>">
                        <source media="(min-width: 1366px)" srcset="<?php the_post_thumbnail_url('laptop-img') ?>">
                        <source media="(min-width: 767px)" srcset="<?php the_post_thumbnail_url('tablet-img') ?>">
                        <source media="(min-width: 576px)" srcset="<?php the_post_thumbnail_url('square-img') ?>">
                        <img src="<?php the_post_thumbnail_url( 'mobile-img' ) ?>">
                    </picture>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12 p-0">
                    <div class="post-heading-wrapper">
                        <a href="<?php the_permalink(); ?>">
                            <div class="post-heading">
                                <h2><?php the_title(); ?></h2>
                            </div>
                        </a>
                        <div class="post-excerpt">
                            <p>                            
                                <?php echo excerpt('30'); ?>
                            </p>
                            <p class="post-details">
                                <span><img class="post__date" src="<?php echo get_template_directory_uri(); ?>/assets/icons/clock.svg"><?php the_date(); ?></span>
                                <span><img class="post__category" src="<?php echo get_template_directory_uri(); ?>/assets/icons/tags.svg"><?php the_category( ',&nbsp;' ); ?></span>
                                <?php if ( has_tag() ): ?>
                                    <span><img class="post__tags" src="<?php echo get_template_directory_uri(); ?>/assets/icons/hashtag.svg"><?php the_tags( '', ',&nbsp;' ); ?></span>
                                <?php endif; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <main class="container white-bg post">
            <div class="row">
                <div class="col-12">
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </main>

    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>