<?php 

/* ------------------------------------
    Funkce pro title a meta tagy
------------------------------------ */

	require 'custom-functions/meta-title.php';


/* ------------------------------------
	Asynchronní načítání skriptů a CSS
------------------------------------ */

	function add_async_forscript($url)
	{
		if (strpos($url, '#asyncload')===false)
			return $url;
		else if (is_admin())
			return str_replace('#asyncload', '', $url);
		else
			return str_replace('#asyncload', '', $url)."' async='async"; 
	}

	add_filter('clean_url', 'add_async_forscript', 11, 1);


/* ------------------------------------
	Registrace stylů a skriptů
------------------------------------ */
	function jko_enqueue() {
		wp_enqueue_style( 'jkocss', get_template_directory_uri() . '/css/styles.css', array(), '1.0.0', 'all' );
		wp_enqueue_script( 'jqueryjs', 'https://code.jquery.com/jquery-3.3.1.min.js', array(), '3.3.1', true );
		wp_enqueue_script( 'jkojs', get_template_directory_uri() . '/js/scripts.js#asyncload', array(), '1.0.0', true );
	}
		
	add_action( 'wp_enqueue_scripts', 'jko_enqueue' );


/* ------------------------------------
	Změna přihlašovací stránky
------------------------------------ */

	function my_login_logo() { ?>
		<style type="text/css">
			.login {
				background: #ececec;
				background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/background.svg);
				background-size: 60px 70px;
			}
			#login h1 a, .login h1 a {
				background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/android-chrome-192x192.png);
				background-repeat: no-repeat;
			}
		</style>
	<?php }
	add_action( 'login_enqueue_scripts', 'my_login_logo' );


/* ------------------------------------
	Registrace menu
------------------------------------ */
	function jko_theme_setup() {
		add_theme_support( 'menus' );

		register_nav_menu( 'primary', 'Header Navigation' );
		register_nav_menu( 'secondary', 'Footer Navigation' );

	}

	add_action( 'init', 'jko_theme_setup' );


/* ------------------------------------
	Registrace Google Maps API klíče
------------------------------------ */
function my_acf_init() {
	acf_update_setting( 'google_api_key', 'AIzaSyAl-u6mMaHwC-27k2RFfDbj_upCEeqSSXM' );
}

add_action( 'acf/init', 'my_acf_init' );


/* ------------------------------------
	Vlastní velikosti obrázků
	Hlavní obrázek
------------------------------------ */
	add_image_size( 'mobile-img', 360, 540, true ); // Mobil
	add_image_size( 'square-img', 570, 570, true ); // Čtvercový obrázek – mobil a desktop
	add_image_size( 'tablet-img', 1024, 1024, true ); // Tablet
	add_image_size( 'laptop-img', 1366, 668, true ); // Laptop
	add_image_size( 'desktop-img', 1920, 980, true ); // Desktop

	
/* ------------------------------------
	Podpora konkrétních funkcí v šabloně
------------------------------------ */
	add_theme_support( 'custom-logo' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array('aside', 'image', 'video' ));
	add_theme_support( 'title-tag' );

	
/* ------------------------------------
	Zákaz částí, které šablony napoužívá v administraci
------------------------------------ */
function remove_menus(){
	remove_menu_page( 'edit-comments.php' );          //Komentáře
}

add_action( 'admin_menu', 'remove_menus' );


/* ------------------------------------
	Limit znaků ve stručném výpise
------------------------------------ */
	function excerpt($limit) {
		return wp_trim_words(get_the_excerpt(), $limit);
	}


/* ------------------------------------
	Zobrazení Facebook events
------------------------------------ */
	function facebook_events() {
		$token = "EAAELoiBZB2J8BAJGvKU1KVXfVCYNJQEZBXZB944WnmdV7Q5pO7t11VnScEc3Ed45wY49sq6VMK7y8Gp7gJeUFiM9uZAbt8nj5R5WIqytU1n26T0fZBEXA3we4GauAnvEsWzzF20KwG5n3Lgz3IECV6C50evFOSKEBuqM1HZCg33kgdUN5VeGhcgIftSsaZBZC1B4jyRwqpmvBnIF3OMnid9IwJxKqF8IM9J3flQV10zQFDSBOZAvofm9F";
		$facebook_link = "https://graph.facebook.com/v3.1/168095126572647/events?time_filter=upcoming&access_token=$token";
		$facebook_events = @file_get_contents($facebook_link);
		
		if ($facebook_events !== FALSE) {
			$fb_decoded = json_decode($facebook_events);
			$events = $fb_decoded->data;

			foreach ($events as $event) {
				$event_array = array(
					"name"=>$event->name,
					"description"=>$event->description,
					"start"=>$event->start_time,
					"end"=>$event->end_time,
					"link"=>"https://www.facebook.com/events/" . $event->id . "/",
					"place_name"=>$event->place->name,
					"place_address"=>$event->place->location->street . ", " . $event->place->location->city,
					"place_map"=>"https://www.google.com/maps/place/" . $event->place->location->street . "+" . $event->place->location->city . "/@" . $event->place->location->latitude . "," . $event->place->location->longitude . ",18z"
				);

				$date = DateTime::createFromFormat('U', strtotime($event_array['start']));
				$date->setTimezone(new DateTimeZone('Europe/Prague'));

				echo "<div class='col-lg-6 p-0'>";
					echo "<div class='event-card'>";
						echo "<div class='event-card-description'>";
							echo "<a href='" . $event_array['link'] . "' target='_blank' rel='noopener'>";
								echo "<h3 class='event-card-heading'>" . $event_array['name'] . "</h3>";
							echo "</a>";

							echo "<div class='event-card-details'>";
								echo "<span><img class='event-card__clock' src='" . get_template_directory_uri() . "/assets/icons/clock.svg' alt=''>" . $date->format('j. n. Y, \v G:i') . "</span>";
								echo "<span><a href='" . $event_array['place_map'] . "' target='_blank' rel='noopener'><img class='event-card__marker' src='" . get_template_directory_uri() . "/assets/icons/map-marker-alt.svg' alt=''>" . $event_array['place_name'] . ", " . $event_array['place_address'] . "</a></span>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				echo "</div>";

				unset($event, $event_array, $date);
			}
		} else {
			echo "<div class='col-lg-12 text-center p-2'><h3>Chyba</h3><p>Chyba aplikačního tokenu</p></div>";
		}

		unset($facebook_events, $fb_decoded, $events);
	}
?>