var gulp = require('gulp');
var sass = require('gulp-sass'); // Requires the gulp-sass plugin
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function(){
  return gulp.src('scss/styles.scss')
    .pipe(sass({ 
      compass: true, 
      outputStyle: 'compressed',
      sourcemap: true,
      sourcemapPath: 'css/'})) // Converts Sass to CSS with gulp-sass
    .on('error', swallowError)
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('css'))
});

gulp.task('watch', function(){
  gulp.watch('scss/*.scss', ['sass']);
  gulp.watch('scss/**/*.scss', ['sass']); 
})

function swallowError(error) {
  console.log(error.toString()) // Details of the error in the console
  this.emit('end')
}