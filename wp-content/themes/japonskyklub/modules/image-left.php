<?php
    $image = get_sub_field('image-left-img');
    $image_size = $image['sizes'];

    $image_animate = get_sub_field('image-left-animate');
    if ($image_animate == true):
        $image_animate_class = "item ";

    else: 
        $image_animate_class = "";

    endif;
?>

<div class="row">
    <div class="col-xl-6 p-0 animation-element">
        <?php if ($image_size != null): ?>
            <div class="img-block-left img-block <?php echo $image_animate_class ?>">
            <picture>
                <source media="(min-width: 1200px)" srcset="<?php echo $image_size['square-img'] ?>">
                <source media="(min-width: 992px)" srcset="<?php echo $image_size['laptop-img'] ?>">
                <source media="(min-width: 767px)" srcset="<?php echo $image_size['tablet-img'] ?>">
                <img src="<?php echo $image_size['square-img'] ?>" alt="">
            </picture>
            </div>
        <?php endif; ?>
    </div>

    <div class="col-xl-6 p-0">
        <div class="block-right">
            <?php if (get_sub_field('image-left-head')): ?>
                <h2>
                    <?php 
                        if(get_sub_field('image-left-icon')):
                            $icon = get_sub_field('image-left-icon');
                            echo "<img class='heading-icon' src='" . get_template_directory_uri() . "/assets/icons/" . $icon . ".svg' alt=''>";
                        endif;

                        the_sub_field('image-left-head');
                    ?>
                </h2>
            <?php endif; ?>
            
            <?php the_sub_field('image-left-text') ?>
        </div>
    </div>
</div>