<?php
    $image = get_sub_field('hero-image-img');
    $image_size = $image['sizes'];
    $image_height = get_sub_field('hero-image-height');
?>

<style>
    .hero {
        height: calc(<?php echo $image_height ?>vh - 96px);
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-12 hero-logo-wrapper">
            <div class="hero-logo">
                <a href="<?php echo get_bloginfo('url')?>">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/logo-bile.svg' ?>" alt="">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 p-0 hero">
            <picture>
                <source media="(min-width: 1920px)" srcset="<?php echo $image_size['desktop-img'] ?>">
                <source media="(min-width: 1366px)" srcset="<?php echo $image_size['laptop-img'] ?>">
                <source media="(min-width: 767px)" srcset="<?php echo $image_size['tablet-img'] ?>">
                <source media="(min-width: 576px)" srcset="<?php echo $image_size['square-img'] ?>">
                <source media="(min-width: 320px)" srcset="<?php echo $image_size['mobile-img'] ?>">
                <img src="<?php echo $image_size['desktop-img'] ?>" alt="">
            </picture>
            <div class="hero-heading">
                <h1>
                    <?php the_sub_field('hero-image-head') ?>
                </h1>
            </div>
        </div>
    </div>
</div>