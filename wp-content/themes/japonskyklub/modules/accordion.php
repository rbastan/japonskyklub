<div class="row accordion-wrapper">
    <div class="col-12">
        <?php if (get_sub_field('accordion-head')): ?>
            <h2>
                <?php 
                    if(get_sub_field('accordion-icon')):
                        $icon = get_sub_field('accordion-icon');

                        echo "<img class='heading-icon' src='" . get_template_directory_uri() . "/assets/icons/" . $icon . ".svg' alt=''>";
                    endif;
                    
                    the_sub_field('accordion-head');
                ?>
            </h2>
        <?php endif; ?>

        <?php the_sub_field('accordion-text') ?>
    </div>

    <?php 
        $accordion_items = get_sub_field('accordion-item');

        foreach ($accordion_items as $accordion_item) {
            $accordion_head = $accordion_item['accordion-item-head'];
            $accordion_text = $accordion_item['accordion-item-text'];

            echo "<div class='col-md-6 p-0'><div class='block-center'>";
            echo "<button class='accordion'><img class='accordion-icon' src='" . get_template_directory_uri() . "/assets/icons/plus.svg' alt=''>" . $accordion_head . "</button>";
            echo "<div class='accordion-text'>" . $accordion_text . "</div>";
            echo "</div></div>";
        }
    ?>
</div>