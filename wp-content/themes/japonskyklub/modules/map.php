<?php 
    $map = get_sub_field('map');
    $map_lat = $map['lat'];
    $map_lng = $map['lng'];
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12 white-bg map p-0" id="map">
            <script>
                function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    center: {<?php echo 'lat:' . $map_lat . ', lng:' . $map_lng ?>},
                    zoom: 16,
                    styles: [ { "featureType": "administrative", "elementType": "all", "stylers": [ { "saturation": "-100" } ] }, { "featureType":
                    "administrative.province", "elementType": "all", "stylers": [ { "visibility": "off" } ] }, { "featureType": "landscape",
                    "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": 65 }, { "visibility": "on" } ] }, { "featureType":
                    "poi", "elementType": "all", "stylers": [ { "saturation": -100 }, { "lightness": "50" }, { "visibility": "simplified" } ]
                    }, { "featureType": "road", "elementType": "all", "stylers": [ { "saturation": "-100" } ] }, { "featureType": "road.highway",
                    "elementType": "all", "stylers": [ { "visibility": "simplified" } ] }, { "featureType": "road.arterial", "elementType": "all",
                    "stylers": [ { "lightness": "30" } ] }, { "featureType": "road.local", "elementType": "all", "stylers": [ { "lightness":
                    "40" } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "saturation": -100 }, { "visibility": "simplified"
                    } ] }, { "featureType": "water", "elementType": "geometry", "stylers": [ { "hue": "#ffff00" }, { "lightness": -25 }, { "saturation":
                    -97 } ] }, { "featureType": "water", "elementType": "labels", "stylers": [ { "lightness": -25 }, { "saturation": -100 } ]
                    } ]
                });
                var image = '<?php echo get_stylesheet_directory_uri(); ?>/assets/images/marker.png';
                var marker = new google.maps.Marker({
                    position: {<?php echo 'lat:' . $map_lat . ', lng:' . $map_lng ?>},
                    map: map,
                    title: 'Japonský klub Olomouc',
                    icon: image
                });
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuhVVp55Cm7vWrtL18WbZHm4Xs2OVdQg0&callback=initMap" async defer>
            </script>
        </div>
    </div>
</div>