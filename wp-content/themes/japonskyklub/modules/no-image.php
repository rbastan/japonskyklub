<div class="row">
    <div class="col-12 text-center">
        <?php if (get_sub_field('no-image-head')): ?>
            <h2>
                <?php 
                    if(get_sub_field('no-image-icon')):
                        $icon = get_sub_field('no-image-icon');
                        echo "<img class='heading-icon' src='" . get_template_directory_uri() . "/assets/icons/" . $icon . ".svg' alt=''>";
                    endif;
                    the_sub_field('no-image-head');
                ?>
            </h2>
        <?php endif; ?>
    </div>
    
    <div class="col-12 no-image">
        <div class="no-image-text">
            <?php the_sub_field('no-image-text') ?>
        </div>
    </div>
</div>