<div class="row">
    <div class="col-12">
        <?php if (get_sub_field('events-head')) { 
            echo "<h2>";
                    if (get_sub_field('events-icon')) {
                        $icon = get_sub_field('events-icon');

                        echo "<img class='heading-icon' src='" . get_template_directory_uri() . "/assets/icons/" . $icon . ".svg' alt=''>";
                    }
                    the_sub_field('events-head');
            echo "</h2>";
        } ?>
    </div>
    <div class="col-12 event-card-wrapper">
        <div class="row events">
            <?php facebook_events() ?>
        </div>
    </div>
</div>