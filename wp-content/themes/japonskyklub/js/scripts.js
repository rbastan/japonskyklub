/*
    Animace oken
*/

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    return (elemTop <= docViewBottom);
}

$(window).scroll(function () {
    $('.item').each(function () {
        if (isScrolledIntoView(this) === true) {
            $(this).addClass('in-view')
        }
    });
	$('.footer-symbol').each(function () {
        if (isScrolledIntoView(this) === true) {
            $(this).addClass('in-view')
        }
    });
});

/*
    Rozbalovací list
*/

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}