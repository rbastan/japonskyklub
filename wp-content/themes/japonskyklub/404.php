<?php
    get_header();
?>

<div class="container">
    <div class="row white-bg">
        <div class="col-12">
            <h1>404</h1>
            <p>Stránka nenalezena</p>
        </div>
    </div>
</div>

<?php
    include 'partials/map.php';
    get_footer();
?>