        <footer class="container-fluid">
            <div class="row">
                <?php
                    include 'partials/footer/footer.php';
                    include 'partials/footer/sponsors.php';
                    include 'partials/footer/copyright.php';
                ?>
            </div>
        </footer>
    </body>
</html>