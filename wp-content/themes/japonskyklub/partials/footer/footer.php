            <div class="col-12 dark-bg">
                <div class="row footer">
                    <div class="col-md-6 col-lg-5 footer-about">
                        <p>Japonský klub Olomouc, z.s.</p>
                        <p>
                            IČO: 26591774<br>
                            E-mail: <a href="mailto:info@japonskyklub.cz" title="E-mail">info@japonskyklub.cz</a><br>
                            Bankovní účet: <a href="https://www.fio.cz/ib2/transparent?a=2300551292" target="_blank" title="Transparentní bankovní účet" rel="noopener">2300551292 / 2010</a>
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-5 footer-contact">
                        <p>Kontaktní adresa</p>
                        <p>
                            Katedra asijských studií FF UP<br>
                            Křížkovského 514/14<br>
                            779 00  Olomouc<br>
                            Česká republika
                        </p>
                    </div>
                    <div class="col-lg-2 footer-links">
                        <p>Odkazy</p>
                        <?php wp_nav_menu(array('theme_location' => 'secondary', 'container' => 'p')); ?>
                    </div>
                </div>
            </div>