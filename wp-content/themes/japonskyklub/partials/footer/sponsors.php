<div class="col-12 sponsors">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3 logo-container">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/up-logo.png" alt="Logo Univerzity Palackého v Olomouci" title="Univerzita Palackého v Olomouci">
            </div>
            <div class="col-12 col-sm-6 col-lg-3 logo-container">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/kas-logo.png" alt="Logo Katedry asijských studií" title="Katedra asijských studií">
            </div>
            <div class="col-12 col-sm-6 col-lg-3 logo-container">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/brnotaku-logo.png" alt="Logo spolku Brněnští otaku" title="Brněnští otaku">
            </div>
            <div class="col-12 col-sm-6 col-lg-3 logo-container">
                <img src="<?php echo get_template_directory_uri()?>/assets/images/kimci-logo.png" alt="Logo korejského klubu Kimči" title="Korejský klub Kimči">
            </div>
        </div>
    </div>
</div>