<div class="col-12 social-menu text-right">
    <a href="https://www.facebook.com/japonsky.klub" target="_blank" title="Facebook" rel="noopener">
        <div class="facebook icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/facebook.svg" alt="Facebook logo">
        </div>
    </a>
    
    <a href="https://www.youtube.com/channel/UCwgYss3QTx8qm9nt_sUzdlQ" target="_blank" title="YouTube" rel="noopener">
        <div class="youtube icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/youtube.svg" alt="YouTube logo">
        </div>
    </a>

    <a href="https://www.instagram.com/jkolomouc/" target="_blank" title="Instagram" rel="noopener">
        <div class="instagram icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icons/instagram.svg" alt="Instagram logo">
        </div>
    </a>
</div>