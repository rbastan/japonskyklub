<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Basic meta data to be ❤️ by Google -->
  <title><?php meta(meta_title); ?></title>
  <meta name="description" content="<?php meta(meta_description); ?>">
  <meta name="author" content="Radim Baštan">
  <meta name="contact" content="<?php bloginfo('admin_email'); ?>">

  <!-- OG meta data -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@JaponskyKlub">
  <meta property="og:title" content="<?php meta(og_title); ?>">
  <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?php echo get_home_url(); ?>">
  <meta property="og:image" content="<?php og_image(); ?>">
  <meta property="og:description" content="<?php meta(og_description); ?>">
  <meta property="og:locale" content="<?php bloginfo('language'); ?>">

  <!-- Preload -->
  <link rel="preload" type="text/css" href="<?php echo get_template_directory_uri()?>/css/styles.css" as="style">

  <!-- Links -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&amp;subset=latin-ext">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/styles.css">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/apple-touch-icon.png">
  <link rel="shortcut icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
  <link rel="shortcut icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/manifest.json">
  <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon/safari-pinned-tab.svg" color="#d92324">
  <meta name="theme-color" content="#DC3023">

  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="<?php echo get_template_directory_uri()?>/js/scripts.js" async></script>
</head>